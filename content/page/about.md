---
title: Despre Pauwlonia
subtitle: Arborele cu cea mai rapida crestere din lume!
comments: false
---

Paulownia este un gen de șase până la 17 specii (în funcție de autoritatea taxonomică) a plantelor cu flori din familia Paulowniaceae, legate și uneori incluse în Scrophulariaceae. Ele sunt prezente în mare parte din China, în sudul nordului Laosului și în Vietnam și sunt cultivate mult timp în altă parte în Asia de Est, în special în Japonia și Coreea. Ele sunt copaci foioase, de 12-15 m (39-49 ft) înălțime, cu frunze mari, în formă de inimă, de 15-40 cm, dispuse în perechi opuse de tulpină. Florile sunt produse la începutul primăverii pe panicule de 10-30 cm lungime, cu o corolă tubulară purpurie asemănătoare unei flori de foxglove. Fructul este o capsulă uscată, care conține mii de semințe minute.


Paulownia fortunei flori și scoarță
Genul, inițial Pavlovnia, dar acum scris de obicei Paulownia, a fost numit în onoarea Anna Paulowna, regina Olandei (1795-1865), fiica țarului Paul al Rusiei. Se numește și "prințesă" din același motiv. 

Paulownia este un colonizator timpuriu al solurilor sterile (cum ar fi după un foc sălbatic la temperaturi înalte), deoarece semințele sale sunt ușor uciși de ciupercile de sol. De fapt, este atât de dificil să începem Paulownia prin sămânță că plantațiile reușesc să cumpere portaltoi sau răsaduri - sau să le propagați. În mod remarcabil, deși semințele, răsadurile și rădăcinile copacilor chiar maturi sunt atât de susceptibili de putregai, lemnul nu este și este utilizat pe scară largă pentru construirea de barci și pentru surfboards.


